--created the trigger that blocks the data update in the column 'update_time_at_the_warehouse'--
create trigger UpdateDates ON product
after UPDATE
as
if UPDATE(update_time_at_the_warehouse)
begin
	  print 'error,you can`t update this column('
      rollback transaction
       
end

--checked trigger--
update product
set update_time_at_the_warehouse = getdate()
where update_time_at_the_warehouse = '1900-01-01 00:00:00.000'
--result:error--

--drop trigger--
drop trigger  UpdateDate

--created the trigger that modifies 'update_time_at_the_warehouse'--
create trigger update_date on product
after insert
as
declare @update_time_at_the_warehouse datetime
select @update_time_at_the_warehouse= update_time_at_the_warehouse from inserted
if @update_time_at_the_warehouse<= getdate()
begin
print 'error insert'
rollback transaction
end

--check--
insert into product (id,name,amount,price,arrival_time_at_the_warehouse,update_time_at_the_warehouse) 
values 
(6,'green apple',100,3.00,'1900-01-01 00:00:00.000','2018-07-18 18:23:21.643');
--result:error--

--create view--
create view Sum_Price 
as
select amount,sum(price)*sum(amount) as Sum_Price
from product
group by amount
go

--check--
select * from Sum_Price

--create view with check option--
create view new_view
as
SELECT *
  FROM product
  WHERE name= 'apple'
  WITH CHECK OPTION
  go

  --check--
insert into new_view (id,name,amount,price,arrival_time_at_the_warehouse)
values
(7,'green',100,3.00,'1900-01-01 00:00:00.000');
--result:error--

--create some tables-

create table student(
gradebook int not null primary key,
surname varchar(20) not null,
name varchar(20) not null,
patronymic varchar(30) not null,
date_of_birth date null,
address varchar(50) not null,
phone char(10) not null UNIQUE,
chair char(2) not null,
specialty varchar(20) not null,
form_of_study char(1) not null,
date_of_admission date not null default getdate(),
date_of_fire date null
)


create table exam(
id_e int not null primary key,
subject varchar(20) not null,
[date] date null,
[time] time null,
audience char(3) null,
gradebook int not null foreign key references student(gradebook) 
)

create table result(
gradebook int not null foreign key references student(gradebook),
id_e int not null foreign key references exam(id_e),
mark decimal(6,2) null
)

insert into student(gradebook,surname,name,patronymic,date_of_birth,address,phone,chair,specialty,form_of_study)
values
(123,'Bas','Igor','Olegovich','1999-07-08','vrub 15','0734945682','IT','programmer','d'),
(321,'Bench','Efir','Petrovich','1998-06-18','aaa 666','0508745682','IT','programmer','n'),
(124,'Git','Oleg','Olegovich','1999-07-28','pasichna 111','0504945682','ET','economic','n'),
(125,'Lukas','Dania','Semenovich','1997-12-12','vod 5','0676785682','ET','economic','d'),
(312,'Kosmos','Sergo','Petrovich','2012-12-12','kerch 1','0734940002','IT','programmer','d');

insert into exam (id_e,subject,[date],[time],audience,gradebook)
values 
(1,'c++','2018-07-18','12:00:00','412',125),
(2,'C#','2018-06-18','13:30:00','412',124),
(3,'BI','2018-07-20','12:20:00','410',321),
(4,'DB','2018-07-17','11:00:00','312',123),
(5,'java','2018-06-28','09:00:00','312',312);

insert into result(gradebook,id_e,mark) values
(125,1,'88'),
(124,2,'72'),
(321,3,'99'),
(123,4,'86'),
(312,5,'90');
------------------------------------------------------------------------------------------------------------
--created 3 triggers(delete,update and insert)--
create trigger insert_data on student
after insert
as
declare @specialty varchar(20)
select @specialty=specialty from inserted
if @specialty= 'programmer'
begin
print 'Error insert'
rollback transaction
end

--check--
INSERT INTO student(gradebook,surname,name,patronymic,date_of_birth,address,phone,chair,specialty,form_of_study) 
values
(122,'Bas','Igor','Olegovich','1999-07-08','vrub 15','0734555682','IT','programmer','d');
--result:error--
drop trigger  insert_data
-------------------------------------------
create trigger update_data ON student
after UPDATE
as
if UPDATE(date_of_birth)
begin
	  print 'error,you can`t update this column('
      rollback transaction
       
end
--check--
update student
set date_of_birth ='2018-07-18'
where surname='Bas'
--result:error--
drop trigger update_date
-----------------------------------------
CREATE TRIGGER delete_data ON student
AFTER DELETE
AS
IF EXISTS (SELECT * FROM deleted WHERE specialty='programmer')
BEGIN
	PRINT 'Error delete'
	ROLLBACK TRANSACTION
END
--check--
delete from student
where specialty='programmer'
--result:error--

--check constraint unique--
update student
set phone ='0734945682'
where gradebook =321
--result:error--

--select some data--
select s.surname,s.name,e.subject,r.mark
from student s
inner join exam e on e.gradebook=s.gradebook
inner join result r  on r.gradebook=s.gradebook
where r.mark >=88.00
order by r.mark desc
--completed successfully--

--synonyms--
create synonym dbo.st for testDB.dbo.student 
go

create synonym dbo.ex for testDB.dbo.exam 
go

create synonym dbo.rl for testDB.dbo.result 
go
--completed successfully--
select * from sys.tables
--------------------------
--#1--
select surname,name,specialty,form_of_study 
from dbo.st
where specialty ='programmer' and form_of_study ='n'
--completed successfully--
--#2--
select subject,time,audience
from dbo.ex
where time>='12:00:00' and audience='410'
--completed successfully--
--#3--
select * from dbo.rl
where mark>=88.00
order by mark
--completed successfully--
--#4--
select st.surname,st.name,st.specialty,ex.subject,rl.gradebook,rl.mark
from dbo.st st
inner join dbo.ex ex on ex.gradebook=st.gradebook
inner join dbo.rl rl on rl.gradebook=st.gradebook
where st.specialty='programmer' and rl.mark>=88
order by rl.mark desc
--completed successfully--
-------------------------------------------------------------------------------------------------------------














