USE master
go
 
CREATE DATABASE [MyDB] ON
PRIMARY
(NAME = 'Main', FILENAME = 'C:\DB\Main.mdf'),
 
FILEGROUP [IgorBas]
(NAME = 'Igor', FILENAME = 'D:\DB\Igor.ndf'),
 
FILEGROUP [NoClearDude]
(NAME = 'N1', FILENAME = 'D:\DB\N1.ndf'),
(NAME = 'N2', FILENAME = 'D:\DB\N2.ndf')
 
LOG ON
(NAME = 'Main_log', FILENAME = 'D:\LOG\Main_log.ldf')
go
--------------------------------------------------------------------------------------------------------------
USE [MyDB]
go
 
CREATE TABLE [SysTable ] (
ID1 int Primary Key,     Name1 nchar(25)
) ON [PRIMARY]
 
CREATE TABLE [Table1 ] (
ID2 int Primary Key,     Name2 nchar(25)
) ON [IgorBas] 
 
CREATE TABLE [Table2 ] (
ID3 int Primary Key,     Name3 nchar(25)
) ON [IgorBas] 
 
CREATE TABLE [Table3 ] (
ID4 int Primary Key,     Name4 nchar(25)
) ON [NoClearDude]
-------------------------------------------------------------------------------------------------------------
USE  [master]
go

DROP DATABASE IF EXISTS [MyDB]
go

CREATE DATABASE [testDB]
ON   PRIMARY
  ( NAME = testDB,  FILENAME = 'D:\DB\testDB.mdf',  
    SIZE = 50MB, MAXSIZE = UNLIMITED,  FILEGROWTH = 3MB )
LOG ON 
  ( NAME = testDB_log, FILENAME = 'D:\DB\testDB_log.ldf', 
    SIZE = 5MB, MAXSIZE = UNLIMITED,  FILEGROWTH = 10% )
go
-------------------------------------------------------------------------------------------------------------
--tronsaction--
use testDB
go

create table customer(
idCustomer int not null primary key,
name nvarchar(25) not null,
cashBalance decimal(10,2) not null
)
create table Book(
idBook int not null primary key,
NameBook nvarchar(50) not null,
Price decimal(10,2) null,
Amount int not null
)
create table [Order](
idOrder int not null primary key,
idCustomer int not null foreign key references customer (idCustomer),
idBook int not null foreign key references Book (idBook),
Amount int not null,
TimeStamp datetime not null,
price decimal(10,2) not null
)

insert into customer(idCustomer,name,cashBalance) values
(125,'Bas','1000'),
(189,'Gladkyy','5500'),
(198,'Markevich','2100');

insert into Book(idBook,NameBook,Price,Amount)values
(4,'Kobzar','600',3),
(5,'portrait of dorian grey book ','450',2),
(6,'Intermezzo','400',1);
