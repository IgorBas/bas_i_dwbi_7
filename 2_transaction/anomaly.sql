create database testDB
go

use testDB
go

drop table if exists sport_nutrition
drop table if exists customers
drop table if exists orders
go

create table sport_nutrition(
id_nutrition int not null primary key,
name_nutrition nvarchar(30) not null,
price decimal(10,2) not null,
amount int not null
)
go

create table customers(
id_customer int not null primary key,
name_customer nvarchar(30) not null,
cash_balance decimal(10,2) not null
)
go

create table orders(
id_order int not null primary key,
id_nutrition int not null foreign key references sport_nutrition(id_nutrition),
id_customer int not null foreign key references customers(id_customer),
amount int not null,
TimeStamp datetime null,
price decimal(10,2) not null
)
go

 

insert into sport_nutrition(id_nutrition,name_nutrition,price,amount) values
(1,'protein','730.00',10),
(2,'creatine','220.00',2),
(3,'bcaa','450.00',15);
go
insert into customers(id_customer,name_customer,cash_balance) values
(111,'Igor','2000'),
(119,'Oleg','1000'),
(125,'Orest','1500');
go
-----------------------------------------------------------------------
declare @customer int=119
declare @sport_nutrition int=2
declare @amount int =5

update customers
set cash_balance=cash_balance-@amount*(select price from sport_nutrition where id_nutrition=2)
where id_customer=@customer
 
insert into orders values
(1,@sport_nutrition,119,5,getdate(),@amount*(select price from sport_nutrition where id_nutrition=@sport_nutrition))

update sport_nutrition
set amount=amount-@amount
where id_nutrition=@sport_nutrition
go

select * from orders
select * from sport_nutrition
select * from customers
go

------------------------------------
declare @customer int=119
declare @sport_nutrition int=2
declare @amount int =1
declare @price decimal(10,2)

select @price=price from sport_nutrition where id_nutrition=@sport_nutrition
if @price*@amount<=(select cash_balance from customers where id_customer=@customer)
if @amount<=(select amount from sport_nutrition where id_nutrition=@sport_nutrition)

Begin

update customers
set cash_balance=cash_balance-@amount*(select price from sport_nutrition where id_nutrition=2)
where id_customer=@customer
 
insert into orders values
(1,@sport_nutrition,119,1,getdate(),@amount*(select price from sport_nutrition where id_nutrition=@sport_nutrition))

update sport_nutrition
set amount=amount-@amount
where id_nutrition=@sport_nutrition
 end
 go

select * from sport_nutrition
select * from customers
select * from orders

-------------------------------------------
declare @customer int=119
declare @sport_nutrition int=2
declare @amount int =5

begin transaction 

update customers
set cash_balance=cash_balance-@amount*(select price from sport_nutrition where id_nutrition=2)
where id_customer=@customer
 
insert into orders values
(1,@sport_nutrition,119,5,getdate(),@amount*(select price from sport_nutrition where id_nutrition=@sport_nutrition))

update sport_nutrition
set amount=amount-@amount
where id_nutrition=@sport_nutrition

select * from customers where id_customer=@customer
select * from sport_nutrition where id_nutrition=@sport_nutrition

if(select cash_balance from customers where id_customer=@customer)<0 or 
(select amount from sport_nutrition where id_nutrition=@sport_nutrition)<0
begin
print 'lack of money or nutrition'
rollback
end
else 
commit
-----shows changes,but doesn`t make them-----
-----tables stay with the previous data------
declare @customer int=119
declare @sport_nutrition int=2
declare @amount int =5

select * from customers where id_customer=@customer
select * from sport_nutrition where id_nutrition=@sport_nutrition
---------------------------------------------------------------------
drop table if exists test
go

create table test(
id int not null,
amount int not null
)
go

insert into test values (1,10)
go

while (select amount from test where id=1)>0
begin 
waitfor delay '00:00:01'
update test set amount=amount-1 where id=1
waitfor delay '00:00:01'
end
go

select * from test
go
--------------------------------------------------------------------------
while (select amount from test where id=1)>0
begin 
begin transaction
waitfor delay '00:00:01'
update test set amount=amount-1 where id=1
waitfor delay '00:00:01'
if (select amount from test where id=1)<0
rollback
else
commit
end
go

select * from test
go
-----------------------------------------
BEGIN TRANSACTION
CREATE TABLE tab1(ID int IDENTITY PRIMARY KEY,  Value int)
INSERT INTO tab1 VALUES(1)
SAVE TRANSACTION T1
SELECT * FROM tab1
 
INSERT INTO tab1 VALUES(2)
SELECT * FROM tab1
 
ROLLBACK TRANSACTION T1
SELECT * FROM tab1
 
INSERT INTO tab1 VALUES(3)
SELECT * FROM tab1
 
ROLLBACK
------------------------------------
--deadlock--
BEGIN TRANSACTION 
UPDATE sport_nutrition
SET Price=300
WHERE id_nutrition=2
 
WAITFOR DELAY '00:00:10'
 
UPDATE customers
SET cash_balance=500
WHERE id_customer=125
COMMIT
--execute in another query--
BEGIN TRANSACTION 
UPDATE customers
SET cash_balance=500
WHERE id_customer=125

WAITFOR DELAY '00:00:10'

UPDATE sport_nutrition
SET Price=300
WHERE id_nutrition=2
Commit
-------------------------------------
--lost update--
BEGIN TRANSACTION 
update sport_nutrition 
set price =price +25
WAITFOR DELAY '00:00:03'
commit
--execute in another query--
BEGIN TRANSACTION 
update sport_nutrition 
set price =price +35
WAITFOR DELAY '00:00:03'
commit
-------------------------------------
--Dirty reads---
--first transaction--
select price from sport_nutrition where id_nutrition=1
--second transaction--
update sport_nutrition
set price='700'
where id_nutrition=1
--first transaction--
select price from sport_nutrition where id_nutrition=1
--second transaction--
rollback
------------------------------
--Non-repeatable reads--
--first transaction--
begin transaction
select * from sport_nutrition where id_nutrition=1
--second transaction--
begin transaction
update sport_nutrition
set price='700'
where id_nutrition=1
commit
--first transaction--
select * from sport_nutrition where id_nutrition=1
commit
------------------------------------------
--Phantom reads--
--first transaction--
select * from customers 
where cash_balance between 700 and 2000
--second transaction--
begin transaction
insert into customers(id_customer,name_customer,cash_balance) values
(113,'Petro','1500');
commit
--first transaction--
select * from customers 
where cash_balance between 700 and 2000
commit
------------------------------------