create view fragmentation_view as
SELECT 
	B.[name] as 'index_name', 
    	OBJECT_NAME(A.[object_id]) as 'table_name', 
   	 A.[index_id], 
    	A.[page_count], 
    	A.[index_type_desc], 
    	A.[avg_fragmentation_in_percent], 
	A.[avg_fragment_size_in_pages],
	A.[avg_page_space_used_in_percent],
    	A.[fragment_count] 
FROM  sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, 'SAMPLED') A 
	INNER JOIN sys.indexes B  
	ON  A.[object_id] = B.[object_id] and A. [index_id] = B. [index_id]
go
select * from fragmentation_view

