use labor_sql
go 

------1------
select row_number() over( order by id_comp, trip_no) num, trip_no, id_comp 
FROM trip
-------------

------2------
select row_number() over(partition by id_comp order by id_comp, trip_no) n, trip_no, id_comp 
FROM trip
-------------
------3------
select model, color, type, price
FROM (
select *, rank() over(partition BY type ORDER BY price) _rank
 FROM Printer
) q
where _rank = 1;
-------------
------4------
select maker 
from (
select maker, rank() over(partition by maker order by model) _rank
from product 
where type ='pc'
)q
where _rank >2

-------------
------5------
select  price FROM(
select DENSE_RANK() OVER(ORDER BY price DESC) d_rank, price 
from PC
) q
where d_rank=2;
-------------
------6------
;with cte as
(
select *, SUBSTRING([name], charindex(' ', [name])+1, 100) as [Surname] from passenger
)

select *, ntile(3)over(order by (surname)) as [group] from cte
go
-------------
------7------
select  *, 
      case when id % 3 = 0 THEN id/3 ELSE id/3 + 1 END AS page_num, 
      CASE WHEN row_total % 3 = 0 THEN row_total/3 ELSE row_total/3 + 1 END AS page_total
from (
      select *, ROW_NUMBER() OVER(ORDER BY price DESC) AS id, 
             COUNT(*) OVER() AS row_total 
      from pc
) q
-------------

------8------
select max_sum, type, date, point 
from (
select MAX(inc) over() AS max_sum, *
from (
  select inc, 'inc' type, date, point FROM Income 
  UNION ALL 
  select inc, 'inc' type, date, point FROM Income_o 
  UNION ALL 
  select out, 'out' type, date, point FROM Outcome_o 
  UNION ALL 
  select out, 'out' type, date, point FROM Outcome 
  )q
)w
where inc = max_sum;
-------------
------9------
SELECT *, avg(price)over() as total_price
FROM (
select *, price - AVG(price) OVER() AS dif_total_price 
	from (
	select *, price - AVG(price) OVER(PARTITION BY speed) AS dif_local_price  from pc 
	)w
)q

-------------