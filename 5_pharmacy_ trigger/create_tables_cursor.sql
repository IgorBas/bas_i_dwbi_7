use triger_fk_cursor;

declare @id as int, @surname as varchar(30),@name as varchar(30);
declare @sql_script as varchar(max);
declare @column_number as int;
declare @counter as int;
declare @table_name as varchar(100);
declare @type as varchar(10)


declare employee_full_name cursor
for 
	select id,surname,name from employee;

open employee_full_name;

fetch next from employee_full_name into @id,@surname,@name;
while @@fetch_status=0
	begin
		set @table_name=@surname+rtrim(@name)+cast(@id as varchar(3))
		set @sql_script='DROP TABLE IF EXISTS '+@table_name +' CREATE TABLE '
		set @counter=1;
		set @column_number=ABS(CHECKSUM(NEWID()))%9+1;
		set @sql_script=@sql_script+@table_name+'('
		while @counter<=@column_number
			begin
				if @counter%2=0
					  set @type='int'
				else
					  set @type='char(10)'					
				set @sql_script=@sql_script+'column'+cast(@counter as varchar(3))+' '+@type+','
				set @counter+=1;
			end;
		set @sql_script=left(@sql_script,len(@sql_script)-1)+')'
		exec(@sql_script);
		fetch next from employee_full_name into @id,@surname,@name;
	end;
close employee_full_name;
deallocate employee_full_name;