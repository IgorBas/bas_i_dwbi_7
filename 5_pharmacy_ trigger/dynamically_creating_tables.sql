use triger_fk_cursor;

declare @table1_name as char(18), @table2_name as char(18);
declare @sql_script_1 as varchar(max), @sql_script_2 as varchar(max);
declare @table_definition as varchar(max);
declare @row as varchar(max);
DECLARE @row_definition as varchar(max);

--column for new 2 tables
set @table_definition='(id INT NOT NULL, surname VARCHAR(30) NOT NULL, name CHAR(30) NOT NULL, midle_name VARCHAR(30),
    identity_number CHAR(10), passport CHAR(10), experience DECIMAL(10, 1), birthday DATE,
    post VARCHAR(15) NOT NULL, pharmacy_id INT)';

set @row_definition='(id,surname,name,midle_name,identity_number,passport,experience,birthday,post,pharmacy_id)'

--tables name
set @table1_name='[table_1_'+cast(cast(current_timestamp as time(0)) as char(8))+']';
set @table2_name='[table_2_'+cast(cast(current_timestamp as time(0)) as char(8))+']';

-------------------------------
set @sql_script_1='create table '+ @table1_name + @table_definition +' '+
				'create table ' + @table2_name + @table_definition+';';

exec(@sql_script_1);
-----------------------------------------
---from  employee insert rows in this 2 tables,the distribution of lines between tables is random
--declare the variables for the cursor
declare @id as INT,@surname as VARCHAR(30),@name as CHAR(30),@midle_name as VARCHAR(30)
declare	@identity_number as CHAR(10),@passport as CHAR(10), @experience as DECIMAL(10, 1)
declare @birthday as DATE, @post as VARCHAR(15),@pharmacy_id as INT;

--declare cursor
declare cursor_for_random_insert cursor
for
	SELECT id,surname,name,midle_name,identity_number,
		   passport,experience,birthday,post,pharmacy_id
	FROM employee;

-----
open cursor_for_random_insert;
fetch next from cursor_for_random_insert into @id,@surname,@name,@midle_name,
											  @identity_number,@passport,@experience,
											  @birthday,@post,@pharmacy_id;



while @@FETCH_STATUS=0
	begin
		set @row=RTRIM(cast(@id as varchar(5)))+','+''''+RTRIM(@surname)+''''+','+''''+RTRIM(@name)+''''+','
			+''''+RTRIM(@midle_name)+''''+','+''''+RTRIM(@identity_number)+''''+','
			+''''+RTRIM(@passport)+''''+','+RTRIM(cast(@experience as varchar(5)))+','+
			''''+RTRIM(cast(@birthday AS VARCHAR(10)))+''''+','
			+''''+RTRIM(@post)+''''+','+RTRIM(cast(@pharmacy_id as varchar(5)))

		if rand()>0.5
			begin
				set @sql_script_2='insert into '+@table1_name+@row_definition+' VALUES('+@row+')'
			end
		else
			begin
				set @sql_script_2='insert into '+@table2_name+@row_definition+' VALUES('+@row+')'
			end
		exec(@sql_script_2)

		fetch next from cursor_for_random_insert into @id,@surname,@name,@midle_name,
											  @identity_number,@passport,@experience,
											  @birthday,@post,@pharmacy_id;
	end
close cursor_for_random_insert;
deallocate cursor_for_random_insert;







