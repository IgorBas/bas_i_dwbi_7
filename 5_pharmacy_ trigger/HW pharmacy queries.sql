USE [master]
GO

CREATE DATABASE [triger_fk_cursor]
GO
USE [triger_fk_cursor]
GO

CREATE TABLE [employee](
    id                 INT               IDENTITY(1,1),
    surname            VARCHAR(30)       NOT NULL,
    name               CHAR(30)          NOT NULL,
    midle_name         VARCHAR(30),
    identity_number    CHAR(10),
    passport           CHAR(10),
    experience         DECIMAL(10, 1),
    birthday           DATE,
    post               VARCHAR(15)       NOT NULL,
    pharmacy_id        INT,
    PRIMARY KEY (id)
)

CREATE TABLE [medicine](
    id               INT            IDENTITY(1,1),
    name             VARCHAR(30)    NOT NULL,
    ministry_code    CHAR(10),
    recipe           BIT,
    narcotic         BIT,
    psychotropic     BIT,
    PRIMARY KEY (id)
)

CREATE TABLE [medicine_zone](
    medicine_id    INT    NOT NULL,
    zone_id        INT    NOT NULL,
    PRIMARY KEY (medicine_id, zone_id)
)

CREATE TABLE [pharmacy](
    id                 INT            IDENTITY(1,1),
    name               VARCHAR(15)    NOT NULL,
    building_number    VARCHAR(10),
    www                VARCHAR(40),
    work_time          TIME,
    saturday           BIT,
    sunday             BIT,
    street             VARCHAR(25),
    PRIMARY KEY (id)
)

CREATE TABLE [pharmacy_medicine](
    pharmacy_id    INT    NOT NULL,
    medicine_id    INT    NOT NULL,
    PRIMARY KEY (pharmacy_id, medicine_id)
)

CREATE TABLE [post](
    post    VARCHAR(15)    NOT NULL,
    PRIMARY KEY (post)
)

CREATE TABLE [street](
    street    VARCHAR(25)    NOT NULL,
    PRIMARY KEY (street)
)

CREATE TABLE [zone](
    id      INT            IDENTITY(1,1),
    name    VARCHAR(25)    NOT NULL,
    PRIMARY KEY (id)
)
--------------------------------------------------------------------------------------------------------------------
--insert data--

insert into employee (surname,name,midle_name,identity_number,passport,experience, birthday,post, pharmacy_id ) 
values
('Bas','Igor','Olegovich','1111111111','1234567890','100.0','1999-07-08','director',5),
('Gladkyy','Oleg','Vasilovich','2222222222','0987654321','90.0','1999-06-24','ex_director',4),
('Markevich','Vlad','Null','3333333333','1234561111','80.0','1999-10-31','worker',3),
('Markevich','Lykiian','Null','4444444444','0101010101','87.0','1999-10-31','worker',2),
('Kosmos','Sergo','Igorovich','5555555555','1212121212','60.0','2001-04-07','worker',1);

insert into medicine(name,ministry_code,recipe,narcotic,psychotropic)
values
('strepsils','9876789545',0,0,0),
('dunabol','1234589545',1,1,0),
('test propionat','1176789545',1,1,1),
('metan','9871119545',1,1,0),
('testosteron','8989898989',1,1,1);

insert into medicine_zone (medicine_id,zone_id) values
(5,1),
(4,2),
(3,3),
(2,4),
(1,5);

insert into [zone] (name)values
('heart'),
('liver'),
('heart'),
('kidneys'),
('throat');

insert into pharmacy(name,building_number,www,work_time,saturday,sunday,street)values
('Apteka','17','apkeka.com','09:00:22:00',1,1,'vrub'),
('Podorojnik','18','pod.com','08:00:21:00',1,1,'vodokanal'),
('Ximik','666','ximik.com','00:00:23:00',0,0,'ko4ka'),
('SamSebePolikyii','1','samsam.com','09:00:10:00',0,0,'nevidoma'),
('Apteka y Panasa','144','panas.com','08:00:21:00',1,1,'batalna');

insert into pharmacy_medicine(pharmacy_id,medicine_id)values
(1,5),
(2,4),
(3,3),
(4,2),
(5,1);

insert into post (post) values 
('director'),
('ex-director'),
('worker');

insert into street (street)values
('vrub'),
('vodokanal'),
('ko4ka'),
('nevoidoma'),
('batalna');
----------------------------------------------------------------------------------------------------------------------
--create trigger for realization foreign key--
CREATE TRIGGER dbo.trg_aTable_IOI
   ON  dbo.aTable
  INSTEAD ,update
AS 
BEGIN
    SET NOCOUNT ON;

    IF (EXISTS(SELECT I.RelationField
    			FROM INSERTED I
    			LEFT JOIN OtherDB.dbo.MasterTable MT ON I.RelationField = MT.ID
    			WHERE I.RelationField IS NOT NULL AND MT.ID IS NULL)
    )
    ROLLBACK --OR raise Exception

    INSERT INTO aTable(fieldList)
    SELECT fieldList FROM INSERTED
END
GO
 










