ALTER trigger tg_authors ON Authors
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end

---- insert
if @operation = 'I'
begin
	insert into Authors_log
				(Author_Id_new,Name_new,URL_new,operation_type,
				book_amount_new,issue_amount_new,total_edition_new)
	Select 
		inserted.Author_Id,
		inserted.Name,
		inserted.URL,
		@operation,
		inserted.book_amount,
		inserted.issue_amount,
		inserted.total_edition

	FROM Authors 
	inner join inserted on Authors.Author_Id = inserted.Author_Id
end

---- delete
if @operation = 'D'
begin
	insert into Authors_log
				(Author_Id_old,Name_old,URL_old,operation_type,
				book_amount_old,issue_amount_old,total_edition_old)
	Select 
		deleted.Author_Id,
		deleted.Name,
		deleted.URL,
		@operation,
		deleted.book_amount,
		deleted.issue_amount,
		deleted.total_edition
	FROM deleted
end

--- update
if @operation = 'U'
	begin
		update Authors 
		set updated = CURRENT_TIMESTAMP, 
			updated_by = SYSTEM_USER
		from Authors 
			inner join inserted on Authors.Author_Id = inserted.Author_Id;

	insert into Authors_log
				(Author_Id_new,Name_new,URL_new,Author_Id_old,Name_old,URL_old,operation_type,
				book_amount_new,issue_amount_new,total_edition_new,
				book_amount_old,issue_amount_old,total_edition_old)
	Select 
		inserted.Author_Id,
		inserted.Name,
		inserted.URL,
		deleted.Author_Id,
		deleted.Name,
		deleted.URL,
		@operation,
		inserted.book_amount,
		inserted.issue_amount,
		inserted.total_edition,
		deleted.book_amount,
		deleted.issue_amount,
		deleted.total_edition
	FROM inserted INNER JOIN deleted ON inserted.Author_Id = deleted.Author_Id
		
	end
END;
GO

EXEC sp_configure 'show expanded  options', 1;
GO
RECONFIGURE ;
GO
EXEC sp_configure 'nested triggers', 1 ;
GO
RECONFIGURE;
GO