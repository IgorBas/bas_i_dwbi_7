use master
go
create database IB_library
go

use IB_library
go

create schema IB_library
go


GO
CREATE SEQUENCE IB_library.seq_ym_library
    START WITH 1  
    INCREMENT BY 1 ;  
GO


create table authors(
Author_Id int primary key not null,
Name varchar(50) unique not null,
URL	varchar(255) not null default('www.author_name.com'),
inserted	date not null default (getdate()),
inserted_by	varchar(50) not null default(system_user),
updated	date,
updated_by	varchar(50),
)
go

create table Publishers(
Publisher_Id int primary key not null,
Name varchar(50) unique not null,
url varchar(50) default 'wwwpublisher_name.com' not null,
seq_no int  default 0 check (seq_no>=0) not null,
inserted date not null default getdate(),
inserted_by	varchar(50) not null default system_user,
updated	date,
updated_by	varchar(50)
)
go

create table books(
ISBN varchar(50) primary key not null,
Publisher_Id int foreign key references publishers(publisher_id) not null,
URL	varchar(255) unique not null,
Price	decimal(15,3) default 0 check (price>=0) not null,
inserted	date not null default getdate(),
inserted_by	varchar(50) default system_user,
updated	date,
updated_by	varchar(50)
)
go


create table BooksAuthors(
BooksAuthors_id	int primary key default 1 check(booksauthors_id >=0) not null,
ISBN varchar(50) unique,
author_id int unique not null,
seq_no int  default 0 check (seq_no>=0) not null,
inserted date not null default getdate(),
inserted_by	varchar(50) not null default system_user,
updated	date,
updated_by	varchar(50),
constraint fk_isbn foreign key(isbn) references books(isbn) on update cascade on delete no action,
constraint fk_author_id foreign key(author_id) references authors(author_id) on update cascade on delete no action
)
go



create table Authors_log(
operation_id  int identity primary key not null,
Author_Id_new int,
Name_new varchar(50),
URL_new varchar(50),
Author_Id_old int,
Name_old varchar(50),
URL_old varchar(50),
operation_type varchar(1) check (operation_type in('I','D','U')) not null,
operation_datetime datetime default getdate() not null
)
go
 

create trigger tg_authors
on authors
after insert, update
as 
begin
declare @author_id int
select @author_id=author_id from inserted
if @author_id is null
select @author_id=author_id from deleted
update authors set updated=getdate(), updated_by=SYSTEM_USER
where author_id=@author_id
end
go



create trigger tg_authors_log
on authors_log
after delete
as 
begin
print 'Deleting to this table is limited' 
rollback
end
go



CREATE VIEW v_authors
AS
SELECT        dbo.authors.*
FROM            dbo.authors

GO


USE IB_library
GO
CREATE synonym s_authors
FOR            dbo.authors

GO


------------------------------------------
--insert data--
select * from authors

insert into authors (Author_Id,Name,URL) values
(1,'shevchenko','sheva.com'),
(2,'bas',default),
(3,'gladkyy',default),
(4,'ukrainka','ukr.com'),
(5,'skovoroda','skovoroda.com'),
(6,'kotliarevckii','kotl.com'),
(7,'markevych',default),
(8,'franko','fr.com'),
(9,'poet',default),
(10,'NePoet','yanepoet.com');

select * from authors
-------------------------
select * from Publishers

insert into Publishers(Publisher_Id,Name,url)values
(1,'Svitanok',default),
(2,'1Veresnya','1v.com'),
(3,'goodmorning',default),
(4,'thisisxarasho',default),
(5,'publisher1','pub1.com'),
(6,'publisher2','pub2.com'),
(7,'publisher3',default),
(8,'bookslike','liker.com'),
(9,'bla','bla.com'),
(10,'blabla',default);

select * from Publishers
---------------------------------
select * from books

insert into books(ISBN,Publisher_Id,URL,Price)values
('111-11111-22222-1',1,'book1.com',10.00),
('222-22222-22222-2',2,'book2.com',20.00),
('333-33333-33333-3',3,'book3.com',30.00),
('444-44444-44444-4',4,'book4.com',40.00),
('555-55555-55555-5',5,'book5.com',50.00),
('666-66666-66666-6',6,'book6.com',60.00),
('777-77777-77777-7',7,'book7.com',70.00),
('888-88888-88888-8',8,'book8.com',80.00),
('999-99999-99999-9',9,'book9.com',90.00),
('000-00000-00000-0',10,'book0.com',99.00);

select * from books
-----------------------------------------------
select * from BooksAuthors

insert into BooksAuthors(BooksAuthors_id,ISBN,author_Id,seq_No) values
(1,'000-00000-00000-0',1,2),
(2,'999-99999-99999-9',2,1),
(3,'888-88888-88888-8',3,3),
(4,'777-77777-77777-7',4,4),
(5,'666-66666-66666-6',5,5),
(6,'555-55555-55555-5',6,7),
(7,'444-44444-44444-4',7,6),
(8,'333-33333-33333-3',8,10),
(9,'222-22222-22222-2',9,9),
(10,'111-11111-22222-1',10,8);

select * from BooksAuthors
---------------------------------------------------------------------------------------
--update data--
select * from authors

update authors
set URL='basok.com'
where Name='bas'

select * from authors
-----------------------
select * from Publishers

update Publishers
set Name='bestpublisher'
where Publisher_Id=5

select * from Publishers
--------------------------
select * from books

update books
set price ='100.00'
where URL='book0.com'

select * from books
---------------------------
--delete data--

SELECT * FROM Authors;

DELETE FROM Authors
WHERE Author_Id>=9;

SELECT * FROM Authors;
--------------------------
select * from Publishers
delete from Publishers
where Name='Svitanok'
select * from Publishers
-----------------------------
select*from books
delete from books
where Publisher_Id >8
select* from books
---------------------------

