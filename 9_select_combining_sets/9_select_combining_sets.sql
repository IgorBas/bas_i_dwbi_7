use labor_sql
go

--------------------------------
--------------1-----------------
;with pc_cte(mm) as (select model  from pc)
select p.model,p.maker from product p
join pc_cte on p.model=mm
--------------------------------
--------------2-----------------
;with avg_cte 
as (select avg(price)as avg_price from pc ),
count_cte
as (select count(model) as count_model from pc )
select distinct maker,
(select avg_price from avg_cte) avg,
(select count_model from count_cte) count
from product
--------------------------------
--------------3-----------------
select region_id,id as place_id,name, region_id as PlaceLevel
from [geography]
where region_id = 1

--------------------------------
--------------4-----------------
;with geography_cte (region_id, place_id, [name], PlaceLevel)
 as
(
select region_id, id, [name], 0 from [geography]
where [name] = 'Ivano-Frankivsk'
union all
select g.region_id, g.id, g.[name], gc.PlaceLevel+1 from [geography] g
inner join geography_cte gc on gc.place_id = g.region_id
)
select * from geography_cte
where PlaceLevel > 0

--------------------------------
--------------5-----------------
;WITH cte(n)
AS
(
  select 1 as n
  union ALL
  select n+1
  from cte
  where n<10000
)
select n from cte
option (maxrecursion 0)
--------------------------------
--------------6-----------------
;WITH cte(n)
AS
(
  select 1 as n
  union ALL
  select n+1
  from cte
  where n<100000
)
select n from cte
option (maxrecursion 0)
--------------------------------
--------------7-----------------
;with dates([date])
as
(select cast(concat(year(getdate()),'0101')as datetime)as [date]
union all
select dateadd(d,1,[date])
from dates
where [date] <concat(year(getdate()),'1231'))
select count([date]) as saturday_sunday
from dates
where datename(dw,[date])='saturday' or datename(dw,[date])='sunday'
OPTION (MAXRECURSION 0);

--------------------------------
--------------8-----------------

select distinct [maker] from product 
where [type] = 'pc' 
and 
maker not in (select distinct [maker] from product where [type] = 'laptop')

--------------------------------
--------------9-----------------
select distinct [maker] from product
where [type] = 'pc'
and 
maker != all (select distinct [maker] from product where [type] = 'laptop')

--------------------------------
--------------10----------------
 select distinct maker from product
 where type = 'pc'
 and not
 maker = any (select distinct maker from product where [type] = 'laptop')
--------------------------------
--------------11----------------
select distinct maker
from product 
where type='PC' and maker in (select  maker
from product
where type='Laptop')
--------------------------------
--------------12----------------
select distinct maker
from product 
where type='PC' and not maker <>all (select  maker
from product
where type='Laptop')
--------------------------------
--------------13----------------
select distinct maker
from product 
where type='PC' and  maker =any (select  maker
from product
where type='Laptop')
--------------------------------
--------------14----------------
select distinct maker from product where type = 'pc'
and maker not in
(
select distinct maker from product where type = 'pc'
and
model != all (select model from pc)
)
--------------------------------
--------------15----------------
select  country ,class
from classes 
where country =all (select country from classes where country ='Ukraine' or  country  not in (select country from classes ))
--------------------------------
--------------16----------------
;with saved_ships as
(select o.ship,b.name,b.date,o.result
from outcomes o 
join battles b ON o.battle = b.name)
select distinct a.ship from saved_ships  a
where upper(a.ship) in
(select upper(ship) from saved_ships b
where b.date <a.date and b.result ='damaged')
--------------------------------
--------------17----------------
select distinct maker from product where type = 'pc' and maker not in
(
select distinct maker from product where product.type = 'pc' and not exists(select * from pc where pc.model = product.model)
)
--------------------------------
--------------18----------------
select maker
from product
where model IN (
select  model
from pc
where speed = (select MAX(speed)from pc)
)
AND maker IN (select maker
from product
where type in('printer','PC')
)
--------------------------------
--------------19----------------
select s.class from outcomes o
join ships s on o.ship=s.name
where o.result='sunk'
union
select c.class from outcomes o
join classes c on o.ship=c.class
where o.result='sunk'
--------------------------------
--------------20----------------
select model ,max(price) as max_
from printer
where price=(select max(price) from printer )
group by model
--------------------------------
--------------21----------------
select p.type,l.model,l.speed from product p
join laptop l on p.model=l.model
where l.speed <all(select pc.speed from pc ,laptop l where l.speed<pc.speed)
--------------------------------
--------------22----------------
;with _cte as
(
select p.maker, pr.price
from product p 
join printer pr on p.model = pr.model 
where color IN  (select  color from printer where color = 'y')
) 
select * from _cte where  price = (select min(price) from _cte)
--------------------------------
--------------23----------------
select  o.battle, c.country, count(o.ship) AS count_ships
from outcomes  o
join ships  s on o.ship=s.[name]
join classes  c on s.class=c.class
group by c.country, o.battle
having count(o.ship)>=2
--------------------------------
--------------24----------------
select distinct maker ,(select count(model) from pc where model in(select model from product where maker=p.maker))pc,
(select count(model) from laptop where model in(select model from product where maker=p.maker))laptop,
(select count(model) from printer where model in(select model from product where maker=p.maker))printer
from product p
--------------------------------
--------------25----------------
;with cte as (
select distinct maker ,
(select count(model) from pc where model in(select model from product where maker=p.maker))pc
from product p)
select maker ,
case 
when pc=0 then 'no'
else 'yes('+cast (pc as varchar(10))+')'
end  pc
from cte
--------------------------------
--------------26----------------
;WITH _cte
AS
(
	SELECT point, [date]
	FROM income_o

	UNION

	SELECT point, [date]
	FROM outcome_o
)

SELECT c.point, c.[date], COALESCE([io].inc,0) AS inc, COALESCE(oo.[out],0) AS [out]
FROM _cte c
  LEFT JOIN income_o AS [io]
    ON c.point=[io].point AND c.[date]=[io].[date]
	 LEFT JOIN outcome_o AS oo
	   ON c.point=oo.point AND c.[date]=oo.[date];
--------------------------------
--------------27----------------
select s.[name], numGuns, bore, displacement, [type], country, launched, c.class from ships s
inner join classes c on s.class = c.class
where (
case  numGuns when 8 then 1 else 0 end +
case  bore when 15 then 1 else 0 end +
case  displacement when 32000 then 1 else 0 end +
case  [type] when 'bb' then 1 else 0 end +
case  country when 'USA' then 1 else 0 end+
case  launched when 1915 then 1 else 0 end +
case  s.class when 'Kon' then 1 else 0 end )
>= 4

--------------------------------
--------------28----------------
;WITH point_date_cte
AS
(
  SELECT point, [date]
  FROM outcome_o
  
  UNION

  SELECT point, [date]
  FROM outcome
),

 outcome_cte
 AS
 (
   SELECT point, [date], SUM([out]) AS [out]
   FROM outcome
   GROUP BY point, [date]
 ),
 out_cte
 AS
 (
   SELECT pdc.point, pdc.[date], COALESCE(oo.[out],0) AS once_day_out, COALESCE(oc.[out],0) AS more_once_out
   FROM point_date_cte pdc
     LEFT JOIN outcome_o AS oo
	   ON pdc.point=oo.point and pdc.[date]=oo.[date]
	     LEFT JOIN outcome_cte AS oc
		   ON pdc.point=oc.point and pdc.[date]=oc.[date]
 )

 SELECT point, [date],
        CASE
		 WHEN once_day_out>more_once_out THEN 'once a day'
		 WHEN once_day_out<more_once_out THEN 'more than once a day'
		 ELSE 'both'
		END AS lider
 FROM out_cte;
--------------------------------
--------------29----------------
select p.maker, p.model, p.[type], coalesce(pc.price, l.price, pr.price) as price 
from product p
left join pc on pc.model = p.model
left join laptop l on l.model = p.model
left join printer pr on pr.model = p.model
where p.maker='B'
go
--------------------------------
--------------30----------------
select name, name as class from
(select name from ships
union
select ship from outcomes) o
inner join classes on o.name = classes.class
go
--------------------------------
--------------31----------------
select class as [count] from
(
select class  from ships
union all
(
select class from outcomes
inner join classes on classes.class = outcomes.ship
)
) a
group by class
having count(class) = 1
go
--------------------------------
--------------32----------------
select [name] from ships where launched < 1942
union
select ship from outcomes
inner join battles on battles.[name] = outcomes.battle and DATEPART(year, battles.[date]) < 1942
go


